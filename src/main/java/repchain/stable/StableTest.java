package repchain.stable;


import repchain.node.NodeHost;
import repchain.node.NodeName;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 向5个节点分别手动发送交易，交易发送间隔为internalMillis，然后经过72小时shutdown
 * 同步构造，发送
 *
 * @author zyf
 * @see StableTestV1 异步构造，发送
 */
public class StableTest {

    public static void main(String[] args) throws InterruptedException {

        CreateAndPostTrans node1 = new CreateAndPostTrans(NodeHost.node1Host, NodeName.NODE1, true);
        CreateAndPostTrans node2 = new CreateAndPostTrans(NodeHost.node2Host, NodeName.NODE2, true);
        CreateAndPostTrans node3 = new CreateAndPostTrans(NodeHost.node3Host, NodeName.NODE3, true);
        CreateAndPostTrans node4 = new CreateAndPostTrans(NodeHost.node4Host, NodeName.NODE4, true);
        CreateAndPostTrans node5 = new CreateAndPostTrans(NodeHost.node5Host, NodeName.NODE5, true);

        CountDownLatch downLatch = new CountDownLatch(5);

        // 每隔10ms构造并发送一条
        node1.start(10L);
        node2.start(10L);
        node3.start(10L);
        node4.start(10L);
        node5.start(10L);

        // 发送交易2天
        TimeUnit.HOURS.sleep(72);
        // 发送交易5分钟
//        TimeUnit.MINUTES.sleep(5);
        // 发送交易120s
//        TimeUnit.SECONDS.sleep(120);

        node1.stop(downLatch);
        node2.stop(downLatch);
        node3.stop(downLatch);
        node4.stop(downLatch);
        node5.stop(downLatch);

        downLatch.await();
    }
}
