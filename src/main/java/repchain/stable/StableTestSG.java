package repchain.stable;

import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.client.RClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.ClientUtil;
import repchain.common.Statistics;
import repchain.common.TranFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author zyf
 */
public class StableTestSG extends RClient {

    // 负载host
    private List hostList;
    // 请求间隔时间 millseconds
    private int thinkTime;
    // 采样时间
    private int sampleTime;
    // 并发数目
    private int concurrency;
    // 期望ctps
    private int expectAvgCtps;
    // 启动时延  seconds
    private int delayStart = 5;
    // 交易构造器
    private TranFactory tranFactory;
    // 要发送的总交易数
    private long tranNumbers = 80000000L;
    // 用来计算tps
    private static LongAdder postReqTranNum = new LongAdder();
    // logging
    private Logger logger = LoggerFactory.getLogger(getClass());

    private ThreadFactory namedThreadFactory;
    private ExecutorService multiThreadPool;


    public StableTestSG(List hostList, int concurrency, int thinkTime, int sampleTime, int expectAvgCtps) {
        this.hostList = hostList;
        this.concurrency = concurrency;
        this.thinkTime = thinkTime;
        this.sampleTime = sampleTime;
        this.expectAvgCtps = expectAvgCtps;
    }

    public static void main(String[] args) throws Exception {
        List<String> hostList = Arrays.asList(args);
        StableTestSG stable = new StableTestSG(hostList, 1, 5, 20, 4000);
        stable.init();
        stable.start();
        stable.caculateTpsAndCtps();
    }

    /**
     *
     */
    private void init() {
        this.namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("stable-pool-%d").build();
        this.multiThreadPool = new ThreadPoolExecutor(hostList.size() * concurrency / 2, hostList.size() * concurrency, 2L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(hostList.size() * concurrency), namedThreadFactory, new ThreadPoolExecutor.DiscardPolicy());
        this.tranFactory = new TranFactory(10000);
    }

    private void start() {
        tranFactory.stableTranPoolStart(expectAvgCtps);
        for (Object host : hostList) {
            startPostTran((String) host);
            logger.info("节点{}开始发送数据", host);
        }
    }

    /**
     * 启动，每个线程都遍历自己的list进行发送交易
     */
    private void startPostTran(String host) {

        ClientUtil clientUtil = new ClientUtil(host, true);
        ConcurrentLinkedQueue<String> tranQueue = tranFactory.getStableTranQueue();
        // 开始发送交易
        for (int i = 0; i < concurrency; i++) {
            multiThreadPool.execute(() -> {
                while (postReqTranNum.longValue() < tranNumbers) {
                    try {
                        if (!tranQueue.isEmpty()) {
                            JSONObject jsonObject = clientUtil.post(tranQueue.poll());
                            if (jsonObject.containsKey("err")) {
                                logger.info("Transaction’s txid is {}, err is {}", jsonObject.getString("txid"), jsonObject.getString("err"));
                            }
                            postReqTranNum.increment();
                        }
                        thinkTime = thinkTime < 0 ? 0 : thinkTime;
                        TimeUnit.MILLISECONDS.sleep(thinkTime);
                    } catch (Exception ex) {
                        logger.error("提交交易错误, errMsg is {}", ex.getMessage(), ex);
                    }
                }
            });
        }
    }

    /**
     * 计算实时的tps、ctps，以及平均tps、ctps
     */
    private void caculateTpsAndCtps() throws InterruptedException {
        List<String> consensusList = Arrays.asList("192.168.0.85:8081", "192.168.0.93:8081", "192.168.0.46:8081", "192.168.0.75:8081");
        new Statistics().caculateTpsAndCtps(consensusList, postReqTranNum, delayStart, sampleTime);
    }

}
