package repchain.stable;


import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.TranFactory;
import repchain.node.NodeHost;
import repchain.common.ClientUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * 向5个节点分别手动发送交易，交易发送间隔为internalMillis，然后经过72小时shutdown
 * 异步构造，发送
 *
 * @author zyf
 * @see StableTest 同步构造，发送
 */
public class StableTestV1 {

    private static Logger logger = LoggerFactory.getLogger(StableTestV1.class);
    /**
     * 使用交易提交的方式，同步或者异步（http）
     */
    private static boolean syncMode = true;

    public static void main(String[] args) throws InterruptedException {

        List<String> hostList = Arrays.asList(NodeHost.node1Host, NodeHost.node2Host, NodeHost.node3Host, NodeHost.node4Host, NodeHost.node5Host);

        ClientUtil[] postClients = new ClientUtil[5];

        for (int i = 0; i < postClients.length; i++) {
            postClients[i] = new ClientUtil(hostList.get(i), syncMode);
        }

        Random clientRandom = new Random();
        TranFactory tranFactory = new TranFactory(10000);

        tranFactory.stableTranPoolStart(4000);

        ThreadFactory postTranFactory = new ThreadFactoryBuilder().setNameFormat("postTran-pool-%d").build();
        ScheduledThreadPoolExecutor postTranPool = new ScheduledThreadPoolExecutor(2, postTranFactory, new ThreadPoolExecutor.AbortPolicy());

        // 固定速率，随机向某个节点发送交易
        postTranPool.scheduleAtFixedRate(() -> {
            try {
                int clientIndex = clientRandom.nextInt(5);
                ConcurrentLinkedQueue<String> tranQueue = tranFactory.getStableTranQueue();
                if (!tranQueue.isEmpty()) {
                    String tranHexString = tranQueue.poll();
                    JSONObject res = postClients[clientIndex].post(tranHexString);
                    logger.info("Transaction id is {}", res.getString("txid"));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }, 2000, 5, TimeUnit.MILLISECONDS);

        // 发送交易2天
        TimeUnit.HOURS.sleep(72);
        // 发送交易5分钟
//        TimeUnit.MINUTES.sleep(5);
        // 发送交易120s
//        TimeUnit.SECONDS.sleep(120);

        tranFactory.stop();
        postTranPool.shutdown();

        while (!tranFactory.getCreateTranPool().isShutdown() || !postTranPool.isShutdown()) {
            // doNothing
        }

    }
}
