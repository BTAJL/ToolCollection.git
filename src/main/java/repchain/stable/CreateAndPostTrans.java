package repchain.stable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rcjava.client.TranPostClient;
import com.rcjava.client.async.TranPostAsyncClient;
import com.rcjava.protos.Peer;
import com.rcjava.tran.TranCreator;
import com.rcjava.util.CertUtil;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.model.Transfer;
import repchain.node.NodeName;

import java.io.File;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 创建并发送交易
 *
 * @author zyf
 * @version 1.0
 */
public class CreateAndPostTrans implements Runnable {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private String host;
    private NodeName nodeName;
    private boolean syncMode;

    private TranPostClient tranPostClient;
    private TranPostAsyncClient tranPostAsyncClient;

    private Peer.CertId certId;
    private Peer.ChaincodeId contractAssetsId = Peer.ChaincodeId.newBuilder().setChaincodeName("ContractAssetsTPL").setVersion(1).build();

    private PrivateKey privateKey;
    private TranCreator tranCreator;

    private static Random random = new Random();

//    private Transfer transfer;

    private ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    public CreateAndPostTrans(String host, NodeName nodeName, boolean syncMode) {

        this.host = host;
        this.nodeName = nodeName;
        this.syncMode = syncMode;

        this.tranPostClient = new TranPostClient(host);
        this.tranPostAsyncClient = new TranPostAsyncClient(host);

        this.certId = Peer.CertId.newBuilder().setCreditCode(nodeName.getCreditCode()).setCertName(nodeName.getNodeName()).build();
        this.privateKey = CertUtil.genX509CertPrivateKey(new File(String.format("jks/%s.jks", nodeName.getNodeTag())), "123", nodeName.getNodeTag()).getPrivateKey();
        this.tranCreator = TranCreator.newBuilder().setPrivateKey(privateKey).setSignAlgorithm("SHA256withECDSA").build();

//        int index = new Random().nextInt(5);
//        this.transfer = new Transfer(nodeName.getCreditCode(), NodeName.values()[index].getCreditCode(), 5);

    }

    /**
     * 发送交易的时间间隔
     *
     * @param internalMillis 发送交易的时间间隔
     */
    public void start(long internalMillis) {
        service.scheduleWithFixedDelay(() -> {
            try {
                Peer.Transaction tran = createTran(random.nextInt(5));
                if (syncMode) {
                    JSONObject jsonObject = tranPostClient.postSignedTran(Hex.encodeHexString(tran.toByteArray()));
                    if (jsonObject.containsKey("err")) {
                        logger.info("Transaction‘s txid is {}, err is {}", jsonObject.getString("txid"), jsonObject.getString("err"));
                    }
                } else {
                    tranPostAsyncClient.postSignedTran(Hex.encodeHexString(tran.toByteArray()));
                }
                // logger.info("Transaction id is {}", tran.getId());
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }, 2000, internalMillis, TimeUnit.MILLISECONDS);
    }

    /**
     * 停止发送交易
     *
     * @param downLatch
     */
    public void stop(CountDownLatch downLatch) {
        service.shutdown();
        for (; !service.isShutdown(); ) {
            // 啥也不干，挂着就好
        }
        downLatch.countDown();
    }

    /**
     * 间隔10ms，持续构造并发送交易到节点处
     */
    @Override
    public void run() {
        while (true) {
            Peer.Transaction tran = createTran(new Random().nextInt(5));
            try {
                if (syncMode) {
                    tranPostClient.postSignedTran(Hex.encodeHexString(tran.toByteArray()));
                } else {
                    tranPostAsyncClient.postSignedTran(Hex.encodeHexString(tran.toByteArray()));
                }
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 构造交易
     *
     * @param index 随机数，随机选个节点，然后向该节点转账
     * @return
     */
    private Peer.Transaction createTran(int index) {
        String tranId = UUID.randomUUID().toString().replace("-", "");
        List<String> params = new ArrayList<>();
        Transfer transfer = new Transfer(this.nodeName.getCreditCode(), NodeName.values()[index].getCreditCode(), 1);
        params.add(JSON.toJSONString(transfer));
        Peer.Transaction tran = tranCreator.createInvokeTran(tranId, certId, contractAssetsId, "transfer", params);
        return tran;
    }

}
