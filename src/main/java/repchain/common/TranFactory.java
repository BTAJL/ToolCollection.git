package repchain.common;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.protos.Peer;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static repchain.common.CreateTranUtil.createTran;

/**
 * 创建交易
 *
 * @author zyf
 * @version 1.0
 */
public class TranFactory {

    //　存放构造好的交易的队列，性能测试用
    private ConcurrentLinkedQueue<Peer.Transaction> performTranQueue = new ConcurrentLinkedQueue<>();
    // 构造存放好的交易队列，稳定性测试用
    private ConcurrentLinkedQueue<String> stableTranQueue = new ConcurrentLinkedQueue<>();

    // 创建交易的线程池
    private static ThreadFactory createTranFactory = new ThreadFactoryBuilder().setNameFormat("createTran-pool-%d").build();
    private static ThreadPoolExecutor createTranPool = new ThreadPoolExecutor(8, 16, 0L, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(10000), createTranFactory, new ThreadPoolExecutor.DiscardPolicy());


    // 要维持的交易数（稳定性测试），要构造的交易数（性能测试）
    private int tranNums;
    private AtomicInteger queueSize = new AtomicInteger(0);

    // 管理createTran的线程池
    private ScheduledExecutorService stableTranCreateService = Executors.newSingleThreadScheduledExecutor();
    // 打印一下交易构造进度
    private ScheduledExecutorService tranCreateProcess = Executors.newSingleThreadScheduledExecutor();

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    public TranFactory(int tranNums) {
        this.tranNums = tranNums;
    }

    /**
     * 稳定性测试时，要往queue里加交易，维持一定数目的交易
     *
     * @param expectAvgCtps 期望的平均ctps
     */
    public void stableTranPoolStart(long expectAvgCtps) {
        long start = System.currentTimeMillis();
        stableTranTask();
        long end = System.currentTimeMillis();
        // 每秒能构造的条数
        long tps = tranNums * 1000 / (end - start);
        // 执行间隔,millis
        long delay = tps * 1000 / expectAvgCtps;
        logger.info("期望的ctps：{}，每秒可构造的交易数：{}，stableTranTask执行间隔为：{}", expectAvgCtps, tps, delay);
        stableTranCreateService.scheduleWithFixedDelay(this::stableTranTask, 0, delay, TimeUnit.MILLISECONDS);
    }

    /**
     * 性能测试时，往queue里加交易，一次性构造指定数目的交易
     */
    public void perfTranPoolStart() {
        String threadName = Thread.currentThread().getName();
        tranCreateProcess.scheduleWithFixedDelay(() -> logger.info("当前线程: {}, 构造的交易数为: {}", threadName, queueSize.get()), 5, 20, TimeUnit.SECONDS);
        performTranTask();
        try {
            tranCreateProcess.shutdown();
            tranCreateProcess.awaitTermination(10, TimeUnit.SECONDS);
//            createTranPool.shutdown();
//            createTranPool.awaitTermination(120, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void performTranTask() {
        queueSize.set(performTranQueue.size());
        while (queueSize.get() < tranNums) {
            createTranPool.execute(() -> {
                // 如果不加这个判断，可能会多构造不少交易
                if (queueSize.getAndIncrement() < tranNums) {
                    Peer.Transaction tran = createTran();
                    performTranQueue.offer(tran);
                }
            });
        }
    }

    private void stableTranTask() {
        queueSize.set(stableTranQueue.size());
        while (queueSize.get() < tranNums) {
            createTranPool.execute(() -> {
                // 如果不加这个判断，可能会多构造不少交易
                if (queueSize.getAndIncrement() < tranNums) {
                    Peer.Transaction tran = createTran();
                    stableTranQueue.offer(Hex.encodeHexString(tran.toByteArray()));
                }
            });
        }
    }

    /**
     * 停止构造交易
     */
    public void stop() {
        createTranPool.shutdownNow();
    }

    public ConcurrentLinkedQueue<Peer.Transaction> getPerformTranQueue() {
        return performTranQueue;
    }

    public ConcurrentLinkedQueue<String> getStableTranQueue() {
        return stableTranQueue;
    }

    public ThreadPoolExecutor getCreateTranPool() {
        return createTranPool;
    }

}
