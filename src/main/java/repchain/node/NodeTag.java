package repchain.node;


import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;

/**
 * 节点的TagName
 *
 * @author zyf
 */
public class NodeTag {

    public static final HashMap<String, String> NODE_TAG_MAP = new HashMap<>();

    static {
        // 默认是与RepChain对应的5个节点和Super_Admin
        NODE_TAG_MAP.put("node1", NodeName.NODE1.getNodeTag());
        NODE_TAG_MAP.put("node2", NodeName.NODE2.getNodeTag());
        NODE_TAG_MAP.put("node3", NodeName.NODE3.getNodeTag());
        NODE_TAG_MAP.put("node4", NodeName.NODE4.getNodeTag());
        NODE_TAG_MAP.put("node5", NodeName.NODE5.getNodeTag());
        NODE_TAG_MAP.put("super_admin", NodeName.SUPER_ADMIN.getNodeTag());

        // 再补充生成11个，总共16节点
        for (int i = 6; i <= 16; i++) {
            String randomStr = RandomStringUtils.randomNumeric(18);
            String nodeName = "node" + i;
            NODE_TAG_MAP.put(nodeName, randomStr + "." + nodeName);
        }

    }

}
