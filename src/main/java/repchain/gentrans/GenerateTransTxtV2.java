package repchain.gentrans;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.protos.Peer;
import com.rcjava.tran.TranCreator;
import com.rcjava.util.CertUtil;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.model.Transfer;

import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * 使用线程池来构造签名交易字符串，并保存到指定的txt文件中
 *
 * @author zyf
 */
public class GenerateTransTxtV2 {

    private static Logger logger = LoggerFactory.getLogger(GenerateTransTxtV2.class);

    private static Transfer transfer = new Transfer("121000005l35120456", "12110107bi45jh675g", 1);

    private static Peer.CertId certId = Peer.CertId.newBuilder().setCreditCode("121000005l35120456").setCertName("node1").build(); // 签名ID
    //这个是给转账交易示范用的，此ID需要与repchain合约部署的一致
    private static Peer.ChaincodeId contractAssetsId = Peer.ChaincodeId.newBuilder().setChaincodeName("ContractAssetsTPL").setVersion(1).build();

    private static PrivateKey privateKey = CertUtil.genX509CertPrivateKey(
            new File("jks/121000005l35120456.node1.jks"),
            "123",
            "121000005l35120456.node1").getPrivateKey();

    private static TranCreator tranCreator = TranCreator.newBuilder()
            .setPrivateKey(privateKey)
            .setSignAlgorithm("sha256withecdsa")
            .build();

    private static int tranNum = 50;
    private static int threadNum = 6;

    private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("genTran-pool-%d").build();
    private static ExecutorService multiThreadPool = new ThreadPoolExecutor(threadNum, 32, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) throws IOException, InterruptedException {

        List<String> synArrayList = Collections.synchronizedList(new ArrayList<>());

        int quotient = tranNum / threadNum;
        int remainder = tranNum % threadNum;

        CountDownLatch downLatch = new CountDownLatch(threadNum);

        for (int i = 0; i < threadNum; i++) {
            final int finalI = i;
            multiThreadPool.execute(() -> {
                for (int j = 0; j < ((finalI != (threadNum - 1)) ? quotient : (remainder + quotient)); j++) {

                    String tranId = UUID.randomUUID().toString().replace("-", "");
                    Peer.Transaction tran = tranCreator.createInvokeTran(tranId, certId, contractAssetsId, "transfer", JSON.toJSONString(transfer));
                    String tranHex = Hex.encodeHexString(tran.toByteArray());
                    synArrayList.add(JSON.toJSONString(tranHex));

                    System.out.println(Thread.currentThread().getName() + ":" + tranId);
                    logger.info("tranId: {}", tranId);
                    System.out.println("synArrayList size is : " + synArrayList.size());
                }
                downLatch.countDown();
            });
        }

        downLatch.await();
        multiThreadPool.shutdown();

        System.out.println("synArrayList size is : " + synArrayList.size());
        FileUtils.writeLines(new File(String.format("record/tran/RepChainApi-jdk13-%s-win.txt", tranNum)), "UTF-8", synArrayList);
    }
}
