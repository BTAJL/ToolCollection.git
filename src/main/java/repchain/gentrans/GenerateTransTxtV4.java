package repchain.gentrans;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.protos.Peer;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

import static repchain.common.CreateTranUtil.createTran;

/**
 * 使用线程池来构造签名交易字符串，并保存到指定的txt文件中，其中随机选取节点私钥来构造
 *
 * @author zyf
 */
public class GenerateTransTxtV4 {

    private static Logger logger = LoggerFactory.getLogger(GenerateTransTxtV4.class);

    //这个是给转账交易示范用的，此ID需要与repchain合约部署的一致
    private static Peer.ChaincodeId contractAssetsId = Peer.ChaincodeId.newBuilder().setChaincodeName("ContractAssetsTPL").setVersion(1).build();

    private static int tranNum = 10000;
    private static int threadNum = 10;

    private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("genTran-pool-%d").build();
    private static ExecutorService multiThreadPool = new ThreadPoolExecutor(threadNum, 32, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) throws IOException, InterruptedException {

        List<String> synArrayList = Collections.synchronizedList(new ArrayList<>());

        int quotient = tranNum / threadNum;
        int remainder = tranNum % threadNum;

        CountDownLatch downLatch = new CountDownLatch(threadNum);

        for (int i = 0; i < threadNum; i++) {

            final int finalI = i;

            multiThreadPool.execute(() -> {
                for (int j = 0; j < ((finalI != (threadNum - 1)) ? quotient : (remainder + quotient)); j++) {

                    Peer.Transaction tran = createTran();
                    String tranId = tran.getId();
                    String tranHex = Hex.encodeHexString(tran.toByteArray());
                    synArrayList.add(JSON.toJSONString(tranHex));

                    System.out.println(Thread.currentThread().getName() + ":" + tranId);
                    logger.info("tranId: {}", tranId);
                    System.out.println("synArrayList size is : " + synArrayList.size());

                }
                downLatch.countDown();
            });
        }

        downLatch.await();
        multiThreadPool.shutdown();

        System.out.println("synArrayList size is : " + synArrayList.size());
        FileUtils.writeLines(new File(String.format("record/tran/RepChainApi-jdk13-%s-node1.txt", tranNum)), "UTF-8", synArrayList);
    }
}
