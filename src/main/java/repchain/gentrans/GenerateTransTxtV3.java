package repchain.gentrans;

import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.client.RClient;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * 基于CreateTranTxr服务，通过restfulApi来获取签名交易字符串，并保存到指定的txt文件中
 *
 * @author zyf
 */
public class GenerateTransTxtV3 extends RClient {

    private static Logger logger = LoggerFactory.getLogger(GenerateTransTxtV3.class);

    private static int tranNum = 50;
    private static int threadNum = 6;

    private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("genTran-pool-%d").build();
    private static ExecutorService multiThreadPool = new ThreadPoolExecutor(tranNum, 32, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) throws IOException, InterruptedException {

        List<String> synArrayList = Collections.synchronizedList(new ArrayList<>());

        int quotient = tranNum / threadNum;
        int remainder = tranNum % threadNum;

        CountDownLatch downLatch = new CountDownLatch(threadNum);

        for (int i = 0; i < threadNum; i++) {
            final int finalI = i;
            multiThreadPool.execute(() -> {
                for (int j = 0; j < ((finalI != (threadNum - 1)) ? quotient : (remainder + quotient)); j++) {
                    String tranHex = new GenerateTransTxtV3().genTran();
                    synArrayList.add(tranHex);
                    System.out.println("synArrayList size is : " + synArrayList.size());
                }
                downLatch.countDown();
            });
        }

        downLatch.await();
        multiThreadPool.shutdown();

        System.out.println("synArrayList size is : " + synArrayList.size());
        FileUtils.writeLines(new File(String.format("record/tran/RepChainApi-jdk13-%s-win.txt", tranNum)), "UTF-8", synArrayList);
    }

    /**
     * 通过createTxr项目来生成jsonString
     *
     * @return
     */
    private String genTran() {
        JSONObject jsonObject = this.getJObject("http://localhost:8080/serialTransfer");
        return jsonObject.getString("txCode");
    }
}
