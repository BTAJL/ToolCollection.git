package repchain.performance;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.client.ChainInfoClient;
import com.rcjava.client.RClient;
import com.rcjava.protos.Peer;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.TranFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author zyf
 */
public class PerformanceV2 extends RClient {

    // 负载host
    private String host;
    // 请求间隔时间 millseconds
    private int thinkTime;
    // 并发数目
    private int concurrency;
    // 每个线程的交易数
    private int tranNum;
    // 启动时延  seconds
    private int delayStart;
    // 采样时间 seconds
    private int sampleTime;
    // 分配给每个线程进行处理
    private ArrayList<List<String>> jobListArray = new ArrayList<>();
    // 用来计算tps
    private AtomicLong postReqTranNum = new AtomicLong(0L);
    // logging
    private Logger logger = LoggerFactory.getLogger(getClass());

    private ThreadFactory namedThreadFactory;
    private ExecutorService multiThreadPool;

    // 并发发送交易的线程
    private CountDownLatch preDownLatch;
    private CountDownLatch tranloadDownLatch;

    private TranFactory tranFactory;

    private PerformanceV2(Builder builder) {
        host = builder.host;
        thinkTime = builder.thinkTime;
        concurrency = builder.concurrency;
        tranNum = builder.tranNum;
        delayStart = builder.delayStart;
        sampleTime = builder.sampleTime;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     *
     */
    public void init() {
        this.preDownLatch = new CountDownLatch(concurrency);
        this.tranloadDownLatch = new CountDownLatch(concurrency);
        this.namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("performance-pool-%d").build();
        this.multiThreadPool = new ThreadPoolExecutor(concurrency, 32, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(tranNum), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
        this.tranFactory = new TranFactory(concurrency * tranNum);
    }

    /**
     * 装载交易数据，为每个线程装一个list
     */
    public boolean loadTranData() throws InterruptedException {
        // 先一次性的灌交易，交易数目为并发数*每个线程要发送的交易数
        tranFactory.perfTranPoolStart();
        ConcurrentLinkedQueue<Peer.Transaction> tranQueue = tranFactory.getPerformTranQueue();
        System.err.println(tranQueue.size());
        for (int i = 0; i < concurrency; i++) {
            new Thread(() -> {
                List<String> tranList = new ArrayList<>(tranNum);
                while (tranList.size() < tranNum && !tranQueue.isEmpty()) {
                    tranList.add(Hex.encodeHexString(tranQueue.poll().toByteArray()));
                }
                jobListArray.add(tranList);
                tranloadDownLatch.countDown();
            }, "loadTranData---" + i).start();
        }
        tranloadDownLatch.await();
        return true;
    }

    /**
     * 启动，每个线程都遍历自己的list进行发送交易
     */
    public void startPostTran() throws InterruptedException {

        // 延迟若干秒启动
        TimeUnit.SECONDS.sleep(delayStart);
//        Performance postClient = this;
        String postUrl = String.format("http://%s/transaction/postTranByString", host);
        // 开始发送交易
        for (int i = 0; i < concurrency; i++) {
            int finalI = i;
            multiThreadPool.execute(() -> {
                        jobListArray.get(finalI).forEach(tran -> {
                            try {
                                JSONObject jsonObject = this.postJString(postUrl, JSON.toJSONString(tran));
                                if (jsonObject.containsKey("err")) {
                                    logger.info("Transaction’s txid is {}, err is {}", jsonObject.getString("txid"), jsonObject.getString("err"));
                                }
                                postReqTranNum.incrementAndGet();
                                try {
                                    thinkTime = thinkTime < 0 ? 0 : thinkTime;
                                    TimeUnit.MILLISECONDS.sleep(thinkTime);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception ex) {
                                logger.error("提交交易错误, errMsg is {}", ex.getMessage(), ex);
                            }
                        });
                        logger.info("tranList_{}_size_{}", finalI, jobListArray.get(finalI).size());
                        preDownLatch.countDown();
                    }
            );
        }
    }

    /**
     * 计算实时的tps、ctps，以及平均tps、ctps
     */
    public void caculateTpsAndCtps() throws InterruptedException {

        // 峰值Tps与峰值Ctps
        AtomicLong maxTps = new AtomicLong(0L);
        AtomicLong maxCtps = new AtomicLong(0L);

        List tpsList = new ArrayList<Long>();
        AtomicLong startReqTranNums = new AtomicLong(0L);
        ScheduledExecutorService tpsService = Executors.newSingleThreadScheduledExecutor();

        // 计算实时tps
        tpsService.scheduleWithFixedDelay(() -> {
            // TPS
            long realReqTranNums = postReqTranNum.get();
            System.err.println(String.format("当前已发送交易数 : %s", realReqTranNums));
            long realTps = (realReqTranNums - startReqTranNums.get()) / sampleTime;
            logger.info("real Tps is : {}", realTps);
            startReqTranNums.set(realReqTranNums);
            tpsList.add(realTps);
            maxTps.getAndUpdate(preTps -> Math.max(preTps, realTps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        ChainInfoClient infoClient = new ChainInfoClient(host);

        List ctpsList = new ArrayList<Long>();
        AtomicLong startConfirmedTranNums = new AtomicLong(0L);
        startConfirmedTranNums.set(infoClient.getChainInfo().getTotalTransactions());
        ScheduledExecutorService ctpsService = Executors.newSingleThreadScheduledExecutor();

        // 计算实时ctps
        ctpsService.scheduleWithFixedDelay(() -> {
            // CTPS
            Peer.BlockchainInfo chainInfo = infoClient.getChainInfo();
            long realTranNums = chainInfo.getTotalTransactions();
            System.err.println(String.format("当前链上交易数 : %s", realTranNums));
            long realCtps = (realTranNums - startConfirmedTranNums.get()) / sampleTime;
            logger.info("real Ctps is : {}", realCtps);
            startConfirmedTranNums.set(realTranNums);
            ctpsList.add(realCtps);
            maxCtps.getAndUpdate(preTps -> Math.max(preTps, realCtps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        // 挂起等待提交交易的线程组结束
        preDownLatch.await();

        // 结束计算ctps的线程
        TimeUnit.SECONDS.sleep(2 * sampleTime);
        // 任务中断，中断这个schedule线程
        tpsService.shutdown();
        ctpsService.shutdown();
        multiThreadPool.shutdown();

        // 平均tps
        logger.info("average Tps is : {}", Arrays.stream(tpsList.toArray()).mapToLong(elem -> (Long) elem).sum() / tpsList.size());
        // 平均ctps
        logger.info("average Ctps is : {}", Arrays.stream(ctpsList.toArray()).mapToLong(elem -> (Long) elem).sum() / ctpsList.size());
        // 峰值Tps
        logger.info("peak Tps is : {}", maxTps.get());
        // 峰值Ctps
        logger.info("peak Ctps is : {}", maxCtps.get());

    }


    public static final class Builder {

        private String host;
        private int thinkTime;
        private int concurrency;
        private int tranNum;
        private int delayStart;
        private int sampleTime;

        private Builder() {
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setThinkTime(int thinkTime) {
            this.thinkTime = thinkTime;
            return this;
        }

        public Builder setConcurrency(int concurrency) {
            this.concurrency = concurrency;
            return this;
        }

        public Builder setTranNum(int tranNum) {
            this.tranNum = tranNum;
            return this;
        }

        public Builder setDelayStart(int delayStart) {
            this.delayStart = delayStart;
            return this;
        }

        public Builder setSampleTime(int sampleTime) {
            this.sampleTime = sampleTime;
            return this;
        }

        public PerformanceV2 build() {
            return new PerformanceV2(this);
        }
    }
}
