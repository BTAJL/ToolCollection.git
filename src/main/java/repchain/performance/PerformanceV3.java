package repchain.performance;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.client.RClient;
import com.rcjava.protos.Peer;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.tuple.MutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.TranFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author zyf
 */
public class PerformanceV3 extends RClient {

    // 负载host
    private String host;
    // 请求间隔时间 millseconds
    private int thinkTime;
    // 并发数目
    private int concurrency;
    // 每个线程的交易数
    private int tranNum;
    // 启动时延  seconds
    private int delayStart;
    // 分配给每个线程进行处理
    private ArrayList<List<String>> jobListArray = new ArrayList<>();
    // 用来计算tps
    private static LongAdder postReqTranNum = new LongAdder();
    // logging
    private Logger logger = LoggerFactory.getLogger(getClass());

    // 存放每个交易的发送时间<Left>，以及请求耗时<Right>
    private ConcurrentHashMap<String, MutablePair<Long, Long>> txidPostTime = new ConcurrentHashMap<>();
    // 存放每个交易的接口响应时间
    private ConcurrentHashMap<String, Long> txidSpendTime = new ConcurrentHashMap<>();

    private ThreadFactory namedThreadFactory;
    private ExecutorService multiThreadPool;

    // 并发发送交易的线程
    private CountDownLatch preDownLatch;
    // 等待交易加载完成
    private CountDownLatch tranloadDownLatch;

    private PerformanceV3(Builder builder) {
        host = builder.host;
        thinkTime = builder.thinkTime;
        concurrency = builder.concurrency;
        tranNum = builder.tranNum;
        delayStart = builder.delayStart;
        preDownLatch = builder.preDownLatch;
        tranloadDownLatch = builder.tranloadDownLatch;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     *
     */
    public void init() {
        this.namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("performance-pool-%d").build();
        this.multiThreadPool = new ThreadPoolExecutor(concurrency, 32, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(tranNum), namedThreadFactory, new ThreadPoolExecutor.DiscardPolicy());
    }

    /**
     * 装载交易数据，为每个线程装一个list
     */
    public void loadTranData() {
        TranFactory tranFactory = new TranFactory(concurrency * tranNum);
        // 先一次性的灌交易，交易数目为并发数*每个线程要发送的交易数
        tranFactory.perfTranPoolStart();
        ConcurrentLinkedQueue<Peer.Transaction> tranQueue = tranFactory.getPerformTranQueue();
        logger.info("{} 准备好的交易数：{}", host, tranQueue.size());
        for (int i = 0; i < concurrency; i++) {
            new Thread(() -> {
                List<String> tranList = new ArrayList<>(tranNum);
                while (tranList.size() < tranNum && !tranQueue.isEmpty()) {
                    tranList.add(Hex.encodeHexString(tranQueue.poll().toByteArray()));
                }
                jobListArray.add(tranList);
                tranloadDownLatch.countDown();
            }, "loadTranData---" + i).start();
        }
    }

    /**
     * 启动，每个线程都遍历自己的list进行发送交易
     */
    public void startPostTran() throws InterruptedException {

        // 延迟若干秒启动
        TimeUnit.SECONDS.sleep(delayStart);
        String postUrl = String.format("http://%s/transaction/postTranByString", host);
        // 开始发送交易
        for (int i = 0; i < concurrency; i++) {
            int finalI = i;
            multiThreadPool.execute(() -> {
                        jobListArray.get(finalI).forEach(tran -> {
                            try {
                                long postTime = System.currentTimeMillis();
                                JSONObject jsonObject = this.postJString(postUrl, JSON.toJSONString(tran));
                                if (jsonObject.containsKey("err")) {
                                    logger.info("Transaction’s txid is {}, err is {}", jsonObject.getString("txid"), jsonObject.getString("err"));
                                }
                                String txid = jsonObject.getString("txid");
                                txidPostTime.put(txid, MutablePair.of(postTime, System.currentTimeMillis() - postTime));
//                                txidSpendTime.put(txid, System.currentTimeMillis() - postTime);
                                postReqTranNum.increment();
                                try {
                                    thinkTime = thinkTime < 0 ? 0 : thinkTime;
                                    TimeUnit.MILLISECONDS.sleep(thinkTime);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception ex) {
                                logger.error("提交交易错误, errMsg is {}", ex.getMessage(), ex);
                            }
                        });
                        logger.info("tranList_{}_size_{}", finalI, jobListArray.get(finalI).size());
                        preDownLatch.countDown();
                    }
            );
        }
    }

    public ExecutorService getMultiThreadPool() {
        return multiThreadPool;
    }

    public static LongAdder getPostReqTranNum() {
        return postReqTranNum;
    }

    public ConcurrentHashMap<String, MutablePair<Long, Long>> getTxidPostTime() {
        return txidPostTime;
    }

    public ConcurrentHashMap<String, Long> getTxidSpendTime() {
        return txidSpendTime;
    }

    public static final class Builder {

        private String host;
        private int thinkTime;
        private int concurrency;
        private int tranNum;
        private int delayStart;
        private CountDownLatch preDownLatch;
        private CountDownLatch tranloadDownLatch;

        private Builder() {
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setThinkTime(int thinkTime) {
            this.thinkTime = thinkTime;
            return this;
        }

        public Builder setConcurrency(int concurrency) {
            this.concurrency = concurrency;
            return this;
        }

        public Builder setTranNum(int tranNum) {
            this.tranNum = tranNum;
            return this;
        }

        public Builder setDelayStart(int delayStart) {
            this.delayStart = delayStart;
            return this;
        }

        public Builder setPreDownLatch(CountDownLatch preDownLatch) {
            this.preDownLatch = preDownLatch;
            return this;
        }

        public Builder setTranloadDownLatch(CountDownLatch tranloadDownLatch) {
            this.tranloadDownLatch = tranloadDownLatch;
            return this;
        }

        public PerformanceV3 build() {
            return new PerformanceV3(this);
        }
    }
}
