package repchain.performance;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.Statistics;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;


/**
 * @author zyf
 */
public class PerformanceTestV3 {

    public static void main(String[] args) throws Exception {

        List<String> hostList = Arrays.asList("192.168.2.69:8081", "192.168.2.69:8081");
        Performancer performancer = new Performancer(hostList, 5, 2, 10000, 2, 5);
        performancer.init();
        performancer.start();
        performancer.caculateTpsAndCtps();

        EventQueue.invokeLater(() -> performancer.setVisible(true));
    }

    private static class Performancer extends JFrame {

        private List<String> hostList;
        // 并发发送交易的总线程 == 节点数 * 并发数
        private CountDownLatch preDownLatch;
        // 加载交易的总线程 == 节点数 * 并发数
        private CountDownLatch tranloadDownLatch;
        // 启动时延  seconds
        private int delayStart = 5;
        // 并发数目
        private int concurrency = 3;
        // 每个线程的交易数
        private int tranNum = 10000;
        // 请求间隔时间 millseconds
        private int thinkTime = 3;
        // 采样时间 seconds, 用来计算tps与ctps
        private int sampleTime = 5;
        private PerformanceV3[] performanceV3s;

        private Logger logger = LoggerFactory.getLogger(getClass());

        public Performancer(List<String> hostList, int delayStart, int concurrency, int tranNum, int thinkTime, int sampleTime) {
            this.hostList = hostList;
            this.delayStart = delayStart;
            this.concurrency = concurrency;
            this.tranNum = tranNum;
            this.thinkTime = thinkTime;
            this.sampleTime = sampleTime;
            this.preDownLatch = new CountDownLatch(hostList.size() * concurrency);
            this.tranloadDownLatch = new CountDownLatch(hostList.size() * concurrency);
            this.performanceV3s = new PerformanceV3[hostList.size()];
        }

        /**
         * 初始化，加载数据
         *
         * @throws InterruptedException
         */
        void init() throws InterruptedException {
            for (int i = 0; i < hostList.size(); i++) {
                int finalI = i;
                String host = hostList.get(i);
                performanceV3s[i] = PerformanceV3.newBuilder()
                        .setHost(host)
                        .setThinkTime(thinkTime) // 每个请求间隔时间mills
                        .setConcurrency(concurrency) // 并发
                        .setTranNum(tranNum) // 每个线程发送交易数
                        .setDelayStart(delayStart) // seconds
                        .setPreDownLatch(preDownLatch)
                        .setTranloadDownLatch(tranloadDownLatch)
                        .build();
                performanceV3s[i].init();
                new Thread(() -> performanceV3s[finalI].loadTranData(), "LoadData---" + finalI + "---" + host).start();
                logger.info("节点{}加载数据...", host);
            }
            // 等待所有线程加载完数据
            tranloadDownLatch.await();
            logger.info("所有节点数据加载完成....");
        }

        /**
         * 开始向各个节点发送交易
         */
        void start() {
            for (int i = 0; i < hostList.size(); i++) {
                int finalI = i;
                new Thread(() -> {
                    try {
                        performanceV3s[finalI].startPostTran();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }, "Performance---" + hostList.get(finalI)).start();
                logger.info("节点{}开始发送数据", hostList.get(i));
            }
        }

        /**
         * 计算实时的tps、ctps，以及平均tps、ctps
         */
        public void caculateTpsAndCtps() throws InterruptedException {

            Statistics statistics = new Statistics(preDownLatch);
            statistics.caculateTpsAndCtps(hostList, PerformanceV3.getPostReqTranNum(), delayStart, sampleTime);

            // 结束计算ctps的线程
            for (PerformanceV3 performanceV3 : performanceV3s) {
                performanceV3.getMultiThreadPool().shutdownNow();
            }

            ArrayList<ArrayList<Long>> tpsAndCtps = new ArrayList<>(2);
            tpsAndCtps.add(new ArrayList<>(statistics.getTpsList()));
            tpsAndCtps.add(new ArrayList<>(statistics.getCtpsList()));
//            ArrayList<Long> tempList = new ArrayList<>(tpsList.size());
//            tempList.addAll(tpsList);
//            tpsAndCtps.add(tempList);
//            tempList = new ArrayList<>(ctpsList.size());
//            tempList.addAll(ctpsList);
//            tpsAndCtps.add(tempList);

            createUI(tpsAndCtps, sampleTime);
        }

        /**
         * 画图
         *
         * @param tpsAndCtps
         * @param sampleTime
         */
        private void createUI(List<ArrayList<Long>> tpsAndCtps, int sampleTime) {

            XYDataset dataset = createDataset(tpsAndCtps, sampleTime);
            JFreeChart chart = createChart(dataset);
            ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
            chartPanel.setBackground(Color.white);
            chartPanel.setVerticalAxisTrace(true);
            chartPanel.setHorizontalAxisTrace(true);

            add(chartPanel);

            pack();
            setTitle("Line chart");
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }

        private XYDataset createDataset(List<ArrayList<Long>> tpsAndCtps, int sampleTime) {

            List<Long> tpsList = tpsAndCtps.get(0);
            XYSeries seriesTps = new XYSeries("tps");
            for (int i = 0; i < tpsList.size(); i++) {
                seriesTps.add(i * sampleTime, tpsList.get(i));
            }

            List<Long> ctpsList = tpsAndCtps.get(1);
            XYSeries seriesCtps = new XYSeries("ctps");
            for (int i = 0; i < ctpsList.size(); i++) {
                seriesCtps.add(i * sampleTime, ctpsList.get(i));
            }

            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(seriesTps);
            dataset.addSeries(seriesCtps);

            return dataset;
        }

        private JFreeChart createChart(final XYDataset dataset) {

            JFreeChart chart = ChartFactory.createXYLineChart(
                    "tps and ctps",
                    "sampleTime",
                    "value",
                    dataset,
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false
            );

            XYPlot plot = chart.getXYPlot();

            XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

            renderer.setSeriesPaint(0, Color.RED);
            renderer.setSeriesStroke(0, new BasicStroke(2.0f));
            renderer.setSeriesPaint(1, Color.BLUE);
            renderer.setSeriesStroke(1, new BasicStroke(2.0f));

            plot.setRenderer(renderer);
            plot.setBackgroundPaint(Color.white);
            plot.setRangeGridlinesVisible(true);
            plot.setRangeGridlinePaint(Color.BLACK);
            plot.setDomainGridlinesVisible(true);
            plot.setDomainGridlinePaint(Color.RED);

            chart.getLegend().setFrame(BlockBorder.NONE);

            chart.setTitle(new TextTitle("tps and ctps", new Font("Serif", Font.BOLD, 18)));

            return chart;
        }

    }

}
