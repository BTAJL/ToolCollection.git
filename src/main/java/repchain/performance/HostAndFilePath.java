package repchain.performance;

/**
 * NodeHost地址以及对应的TransactionFilePath
 *
 * @author zyf
 */
public class HostAndFilePath {

    private String host;
    private String filePath;

    public HostAndFilePath() {
    }

    public HostAndFilePath(String host, String filePath) {
        this.host = host;
        this.filePath = filePath;
    }

    private HostAndFilePath(Builder builder) {
        setHost(builder.host);
        setFilePath(builder.filePath);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(HostAndFilePath copy) {
        Builder builder = new Builder();
        builder.host = copy.getHost();
        builder.filePath = copy.getFilePath();
        return builder;
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static final class Builder {
        private String host;
        private String filePath;

        private Builder() {
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public HostAndFilePath build() {
            return new HostAndFilePath(this);
        }
    }
}
