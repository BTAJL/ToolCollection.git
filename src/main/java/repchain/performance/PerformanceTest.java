package repchain.performance;

import java.io.IOException;

/**
 * @author zyf
 */
public class PerformanceTest {

    public static void main(String[] args) throws IOException, InterruptedException {

        Performance per = Performance.newBuilder()
                .setHost("192.168.2.86:8081")
                .setThinkTime(5) // 每个请求（同步）间隔时间mills
                .setConcurrency(10) // 并发
                .setSampleTime(5) // 采样时间 seconds
                .setDelayStart(5) // seconds
                .setTranFilePath("record/tran/RepChainApi-jdk13-200000-win.txt")
                .build();

        per.init();
        per.loadTranData();
        per.startPostTran();
        per.caculateTpsAndCtps();
        per.caculateDelay();

    }
}
