package repchain.performance;


/**
 * @author zyf
 */
public class PerformanceTestV2 {

    public static void main(String[] args) {

        HostAndFilePath[] hfps = new HostAndFilePath[5];

        hfps[0] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8081").build();
        hfps[1] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8081").build();
        hfps[2] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8085").build();
        hfps[3] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8087").build();
        hfps[4] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8089").build();

        int nodeHostNum = 2;

        // 也可分别设置
        for (int i = 0; i < nodeHostNum; i++) {
            int finalI = i;
            new Thread(() -> {
                try {
                    new Performancer(hfps[finalI]).start();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "Performance---" + finalI).start();
            System.err.println("123456--------------" + finalI);
        }

    }


    static class Performancer {

        private HostAndFilePath hostAndFilePath;

        Performancer(HostAndFilePath hostAndFilePath) {
            this.hostAndFilePath = hostAndFilePath;
        }

        /**
         * 使用默认的参数
         *
         * @throws InterruptedException
         */
        void start() throws InterruptedException {
            start(5, 3, 10000, 5, 5);
        }

        /**
         * 往多个节点同时压测时候，在开启计算时延的情况下，一定要将并发设置为一样，如果不开启计算时延，可以并发不一样
         *
         * @param thinkTime   请求间隔时间, mills
         * @param concurrency 并发请求数
         * @param tranNum     每个线程发送1w条交易
         * @param sampleTime  采样间隔时间，用来计算tps与ctps
         * @param delayStart  启动延时, seconds
         * @throws InterruptedException
         */
        void start(int thinkTime, int concurrency, int tranNum, int sampleTime, int delayStart) throws InterruptedException {

            PerformanceV2 per = PerformanceV2.newBuilder()
                    .setHost(hostAndFilePath.getHost())
                    .setThinkTime(thinkTime) // 每个请求间隔时间mills
                    .setConcurrency(concurrency) // 并发
                    .setTranNum(tranNum) // 每个线程发送1w条交易
                    .setSampleTime(sampleTime) // 采样时间 seconds
                    .setDelayStart(delayStart) // seconds
                    .build();

            per.init();
            per.loadTranData();
            per.startPostTran();
            per.caculateTpsAndCtps();

        }
    }
}
