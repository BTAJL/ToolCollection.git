package repchain.function;

import com.alibaba.fastjson.JSONObject;
import com.rcjava.client.ChainInfoClient;
import com.rcjava.client.TranPostClient;
import com.rcjava.protos.Peer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.CreateTranUtil;

import java.util.concurrent.TimeUnit;

/**
 * 功能性的简单测试
 *
 * @author zyf
 */
public class FunctionTest {

    final static ChainInfoClient infoClient = new ChainInfoClient("localhost:8081");
    final static TranPostClient postClient = new TranPostClient("localhost:8081");
    static Logger logger = LoggerFactory.getLogger(FunctionTest.class);

    public static void main(String[] args) throws InterruptedException {

        long tranStart = infoClient.getChainInfo().getTotalTransactions();
        Runnable correct = () -> {
            for (int i = 0; i < 10000; i++) {
                Peer.Transaction tran = CreateTranUtil.createTran();
                Peer.Transaction tranDup = CreateTranUtil.createDuplicateTran(tran.getId());

                JSONObject res = postClient.postSignedTran(tran);
                JSONObject resDup = postClient.postSignedTran(tranDup);

                if (res.getString("err") != null) {
                    logger.error("correct transaction，err: {}, id: {}", res.getString("err"), res.getString("txid"));
                } else {
                    logger.info("correct transaction，id: {}", res.getString("txid"));
                }

                if (resDup.getString("err") != null) {
                    logger.error("duplicate transaction, err: {}, id: {}", resDup.getString("err"), resDup.getString("txid"));
                } else {
                    logger.info("duplicate transaction, id: {}", resDup.getString("txid"));
                }
                System.err.println("correct over " + i);
            }
        };

        Runnable error = () -> {
            for (int i = 0; i < 10000; i++) {
                Peer.Transaction tran = CreateTranUtil.createErrorTran();
                JSONObject resError = postClient.postSignedTran(tran);
                if (resError.getString("err") != null) {
                    logger.error("error transaction, err: {}, id: {}", resError.getString("err"), resError.getString("txid"));
                } else {
                    logger.info("error transaction, id: {}", resError.getString("txid"));
                }
                System.err.println("error over " + i);
            }
        };

        Thread correctT = new Thread(correct);
        Thread errorT = new Thread(error);

        correctT.start();
        errorT.start();
        correctT.join();
        errorT.join();

        TimeUnit.SECONDS.sleep(10);
        long tranEnd = infoClient.getChainInfo().getTotalTransactions();

        logger.info("上链交易数为: {}", tranEnd - tranStart);
    }

}
