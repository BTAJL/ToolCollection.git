package repchain.ssh;

/**
 * @author zyf
 */
public class RcServerInfo {

    private String ip;
    private int port;
    private String userName = "root";
    private String password = "iscas";

    public RcServerInfo() {
    }

    public RcServerInfo(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public RcServerInfo(String ip, int port, String userName) {
        this.ip = ip;
        this.port = port;
        this.userName = userName;
    }

    public RcServerInfo(String ip, int port, String userName, String password) {
        this.ip = ip;
        this.port = port;
        this.userName = userName;
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
