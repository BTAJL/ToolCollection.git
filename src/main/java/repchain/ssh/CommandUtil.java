package repchain.ssh;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;


import java.io.*;

/**
 * @author zyf
 */
public class CommandUtil {



    /**
     * 执行命令
     *
     * @param rcServerInfo 服务器用户名、密码、ip、port
     * @param cmd          命令
     * @throws JSchException
     * @throws InterruptedException
     */
    public static void execCmd(RcServerInfo rcServerInfo, String cmd) throws JSchException, InterruptedException, IOException {

        JSch jsch = new JSch();

        Session session = jsch.getSession(rcServerInfo.getUserName(), rcServerInfo.getIp(), rcServerInfo.getPort());

        //set auth info interactively
        // 使用了默认的密码iscas，如果需要修改，到RcUserInfo中修改即可
        session.setUserInfo(rcServerInfo.getPassword().equals("iscas") ? new RcUserInfo() : new RcUserInfo(rcServerInfo.getPassword()));

        session.connect();

        ChannelExec ec = (ChannelExec) session.openChannel("exec");

        ec.setCommand(cmd);
        ec.setInputStream(null);
        ec.setErrStream(System.err);
        ec.setOutputStream(System.out);
        ec.connect();

        byte[] tmp = new byte[1024];
        InputStream in = ec.getInputStream();
        BufferedInputStream bin = new BufferedInputStream(in);
        OutputStream out = ec.getOutputStream();
        InputStream err = ec.getErrStream();

        while (!ec.isClosed() || bin.available() > 0) {
            int i = bin.read(tmp, 0, 1024);
            if (i < 0) break;
            System.out.print(new String(tmp, 0, i));
            Thread.sleep(1000);
        }

        while (!ec.isClosed() || err.available() > 0) {
            int i = err.read(tmp, 0, 1024);
            if (i < 0) break;
            System.err.print(new String(tmp, 0, i));
            Thread.sleep(1000);
        }

        System.out.println("exit-status: " + ec.getExitStatus());

        bin.close();
        in.close();
        out.close();
        err.close();

        ec.disconnect();
        session.disconnect();

    }

    public static void main(String[] args) throws InterruptedException, JSchException, IOException {

        RcServerInfo rcServerInfo = new RcServerInfo("192.168.2.xxx", 22, "root", "iscas");
//        CommandUtil.execCmd(rcServerInfo, "yum install -y unzip");
        CommandUtil.execCmd(rcServerInfo, "cd /home/iscas/ && ls && mkdir repchain");

    }

}
