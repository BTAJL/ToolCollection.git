package repchain.ssh;

import com.jcraft.jsch.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Vector;

/**
 * @author zyf
 */
public class ConfigUtil {

    private static String hostNamePrefix = "";
    private static String sysTagPrefix = "";

    public ConfigUtil() {
    }

    public static List<String> readSystemConfig() throws IOException {
        // 读取要修改的配置文件以及要分发的ip段
        List<String> confLineList = FileUtils.readLines(new File("deploy/system.conf"), Charset.defaultCharset());
        return confLineList;
    }

    public static List<String> readSystemConfig(String path) throws IOException {
        // 读取要修改的配置文件以及要分发的ip段
        List<String> confLineList = FileUtils.readLines(new File(path), Charset.defaultCharset());
        return confLineList;
    }

    public static List<String> readStartShConfig() throws IOException {
        // 读取要修改的配置文件以及要分发的ip段
        List<String> startLineList = FileUtils.readLines(new File("deploy/repchain_preview.sh"), Charset.defaultCharset());
        return startLineList;
    }

    public static List<String> readStartShConfig(String path) throws IOException {
        // 读取要修改的配置文件以及要分发的ip段
        List<String> startLineList = FileUtils.readLines(new File(path), Charset.defaultCharset());
        return startLineList;
    }

    public static List<String> readHost() throws IOException {
        // 读取host.txt，用来修改system.conf中的主机ip地址
        List<String> confLineList = FileUtils.readLines(new File("deploy/host.txt"), Charset.defaultCharset());
        return confLineList;
    }

    public static List<String> readHost(String path) throws IOException {
        // 读取host.txt，用来修改system.conf中的主机ip地址
        List<String> confLineList = FileUtils.readLines(new File(path), Charset.defaultCharset());
        return confLineList;
    }

    public static List<String> readNodeName() throws IOException {
        // 读取node.txt，用来修改启动时候的节点别名
        List<String> confLineList = FileUtils.readLines(new File("deploy/node.txt"), Charset.defaultCharset());
        return confLineList;
    }

    public static List<String> readNodeName(String path) throws IOException {
        // 读取node.txt，用来修改启动时候的节点别名
        List<String> confLineList = FileUtils.readLines(new File(path), Charset.defaultCharset());
        return confLineList;
    }

    public static void uploadConfig(RcServerInfo rcServerInfo, String srcPath, String destPath) throws JSchException, SftpException {

        JSch jsch = new JSch();

        Session session = jsch.getSession(rcServerInfo.getUserName(), rcServerInfo.getIp(), rcServerInfo.getPort());

        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(rcServerInfo.getPassword());

        session.connect();

        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect();

        sftpChannel.cd(destPath);

        @SuppressWarnings("unchecked")
        Vector<ChannelSftp.LsEntry> vector = sftpChannel.ls("./");
        for (ChannelSftp.LsEntry entry : vector)
            System.out.println(entry.getFilename() + "    " + entry.getAttrs().getSize());

        sftpChannel.put(srcPath, destPath);

        sftpChannel.disconnect();
        session.disconnect();
        System.out.println("done");
    }

    public static void main(String[] args) throws JSchException, SftpException {
        RcServerInfo rcServerInfo = new RcServerInfo("192.168.2.xxx", 22, "root", "iscas");
        uploadConfig(rcServerInfo, "D:\\jdk13Performance\\repchain.zip", "/home/iscas/test");
    }

}
